# eCommerce-OrderManagement

Sample eCommerce application with microservice architecture. This repo contains Order-Management microservice.

## Tech used

* Spring Boot
* MySql
* Angular
* JWT

## ToDo

| Status | Description |
| --- | --- |
|  ☑️ | Create Orders Controller with necessary endpoints to create, update and list orders. |
|☑️| Extend the class WebSecurityConfigurerAdapter and configure HttpSecurity.|
|☑️| Override the configure method, disable CSRF, Authorize "../authenticate" endpoint for all users.|
|☑️| Set SessionCreationPolicy as StateLess|
|☑️| Add a new Filter class by extending the OncePerRequestFilter class. It should execute before UsernamePasswordAuthenticationFilter Class|
|☑️| Override the doFilterInternal method inside Filter class.|
|☑️| The doFilterInternal method would get the Authorization Header and validate the JWT Token.|
| ☑️| Validate the JWT Token inside doFilterInternal. Throw appropriate errors for invalid/expired tokens|
|☑️ | Get the current Signed in user inside OrderController, create orders for the current user. |
|☑️| Implement the HttpInterceptor interface in UI, send the JWT token to the server in all requests. |
|moved to separate repo ☑️| Create UI components for Create-Orders, Order-List, Delete Orders |