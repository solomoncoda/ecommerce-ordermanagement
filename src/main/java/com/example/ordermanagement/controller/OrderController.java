package com.example.ordermanagement.controller;


import com.example.ordermanagement.models.Order;
import com.example.ordermanagement.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @PostMapping("/order/create")
    public Boolean createOrder(@RequestBody Order order){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        order.setUserName(auth.getName());
        orderRepository.save(order);
        return true;
    }

    @GetMapping("/order/{orderId}")
    public Order getOrderById(@PathVariable Integer orderId){
        return orderRepository.findByorderId(orderId);
    }

    @GetMapping("/order/orders")
    public List<Order> getOrdersForUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return orderRepository.findByuserName(auth.getName());
    }

    @DeleteMapping("/order/{orderId}")
    public Boolean deleteOrder(@PathVariable int orderId){
        orderRepository.deleteById(orderId);
        return true;
    }

}
