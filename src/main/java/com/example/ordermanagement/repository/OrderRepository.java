package com.example.ordermanagement.repository;

import com.example.ordermanagement.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    Order findByorderId(Integer orderId);
    List<Order> findByuserName(String userName);
}
