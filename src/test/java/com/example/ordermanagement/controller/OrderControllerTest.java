package com.example.ordermanagement.controller;

import com.example.ordermanagement.models.Order;
import com.example.ordermanagement.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private OrderController orderController;

    @Mock
    private OrderRepository orderRepository;

    @BeforeEach
    void testSetUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();

        Authentication authentication = Mockito.mock(Authentication.class);
// Mockito.whens() for your authorization object
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    @WithUserDetails("test")
    public void testCreateORDER() throws Exception {
        Order mockOrder = new Order();
        mockOrder.setProductName("test");
        mockOrder.setShippingAddress("test");
        String orderJson = "{\"productName\":\"test\",\"shippingAddress\":\"test\"}";
        Mockito.when(orderRepository.save(mockOrder)).thenReturn(mockOrder);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/order/create")
                .accept(MediaType.APPLICATION_JSON).content(orderJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    void getOrderById() throws Exception {
        Order mockOrder = new Order();
        mockOrder.setProductName("test");
        mockOrder.setShippingAddress("test");
        String orderJson = "{\"productName\":\"test\",\"shippingAddress\":\"test\"}";
        Mockito.when(orderRepository.findByorderId(1)).thenReturn(mockOrder);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/order/1")
                .accept(MediaType.APPLICATION_JSON).content(orderJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    void getOrdersForUser() throws Exception {
        Order mockOrder = new Order();
        mockOrder.setProductName("test");
        mockOrder.setShippingAddress("test");
        String orderJson = "{\"productName\":\"test\",\"shippingAddress\":\"test\"}";
        Mockito.when(orderRepository.findByuserName("test")).thenReturn(new ArrayList<>());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/order/orders")
                .accept(MediaType.APPLICATION_JSON).content(orderJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    void deleteOrder() throws Exception {
        Order mockOrder = new Order();
        mockOrder.setProductName("test");
        mockOrder.setShippingAddress("test");
        String orderJson = "{\"productName\":\"test\",\"shippingAddress\":\"test\"}";
        //Mockito.when(orderRepository.deleteById(1);).thenReturn(new ArrayList<>());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/order/1")
                .accept(MediaType.APPLICATION_JSON).content(orderJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }
}